package com.nespresso.sofa.interview.cart.model;

import static java.util.UUID.randomUUID;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public final class Cart implements Serializable {
    private static final String PROMOTION ="9000";
    public static final String PRODUCT_WITH_PROMOTION = "1000";
    public static String CART="Cart ";
    public static String OPENSENTENCE="{";
    public static String ID="id: ";
    public static String PRODUCTS=", products: ";
    public static String CLOSESENTENCE="}";
    public static Boolean PRODUCTADDED=true;
    public static Boolean PRODUCTNOTADDED=false;
    public static Integer MINPRODUCTQUANTITYALOWED=1;
    public static Integer EMPTY=0;
    public static Integer QUANTITYTOGIVEAGIFT=10;
    public static final String GIFT = "7000";

    private final UUID id;
    private Map<String, Integer> products;

    public Cart() {
        this(randomUUID());
        this.products=new HashMap<>();
    }

    public Cart(UUID id) {
        this.id = id;
        this.products=new HashMap<>();
    }

    public Cart(Map<String, Integer> products) {
        this.id = randomUUID();
        if(doesListContainsProductWithPromotion(products))
            addPromotionAndProducts(products);
        else if(!doesListContainsPromotionProduct(products)){
            addProducts(products);
        }
        else
           initListProducts();

    }



    public UUID getId() {
        return id;
    }

    public Map<String, Integer> getProducts() {
        return this.products;
    }

    public Boolean addProduct(String productCode, int quantity)
    {
        if(quantity<MINPRODUCTQUANTITYALOWED)
            return PRODUCTNOTADDED;
        else {
            if(!doesContainsProduct(productCode))
            this.products.put(productCode, quantity);
            else addToQuantity( productCode, quantity);

            return PRODUCTADDED;
        }
    }



    public  Boolean setProductQuantity(String productCode, int quantity) {

            if (doesContainsProduct(productCode))
                return updateExistingProduct(productCode,quantity);
            else
            return addProduct(productCode,quantity);

    }

    private Boolean doesListContainsProductWithPromotion(Map<String, Integer> products)
    {
        return products.containsKey(PRODUCT_WITH_PROMOTION);
    }

    private Boolean doesListContainsPromotionProduct(Map<String, Integer> products)
    {
        return products.containsKey(PROMOTION);
    }



    private void addToQuantity(String productCode,int quantity)
    {
        int newQuantity=this.products.get(productCode)+quantity;
        this.products.replace(productCode,newQuantity);
    }

    private Boolean updateExistingProduct(String productCode, int quantity)
    {
        if(isQuantityTheSame(productCode,quantity))
            return false;
        else
        { updateQuantity(productCode,quantity);
            return true;
        }
    }

    private void updateQuantity(String productCode, int quantity)
    {
        this.products.replace(productCode, quantity);
        removeIfQuantityEqualsEmpty(productCode);
    }

    private Boolean isQuantityTheSame(String productCode,int quantity)
    {
        return this.products.get(productCode).equals(quantity);
    }

    private Boolean doesContainsProduct(String productCode)
    {
        return this.products.containsKey(productCode);
    }

    private void removeIfQuantityEqualsEmpty(String productCode)
    {
     if(this.products.get(productCode)<=EMPTY)
         this.products.remove(productCode);
    }

    private void addPromotionAndProducts(Map<String, Integer> products)
    {
        this.products = products;
        if(!doesListContainsPromotionProduct(this.products))
            addProduct(PROMOTION,1);
    }

    private void addProducts(Map<String, Integer> products)
    {
        this.products = products;

    }
    private void initListProducts() {
        this.products=new HashMap<>();
    }


    private Boolean isProductNeedGift(int quantity)
    {
        return (quantity>=QUANTITYTOGIVEAGIFT);
    }

    private int calculateGiftsNumber(int quantity)
    {
        return quantity/QUANTITYTOGIVEAGIFT;
    }


    private void addGifts(int numberOfGifts)
    {
        this.products.put(GIFT,numberOfGifts);
    }

    @Override
    public String toString() {

        return new StringBuilder().append(CART).append(OPENSENTENCE).append(ID).append(this.id).append(PRODUCTS)
                                            .append(this.products).append(CLOSESENTENCE).toString();

    }

}
